﻿using Microsoft.AspNetCore.Mvc;
using PruebaTecnica.Data.Entities;
using PruebaTecnica.Data.Respositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PruebaTecnica.Controllers
{
    public class ProfesoresController : Controller
    {
        private readonly IProfesorRepository _profesorRepository;

        public ProfesoresController(IProfesorRepository profesorRepository)
        {
            _profesorRepository = profesorRepository;
        }




        public IActionResult Index()
        {
            var Profesores = _profesorRepository.GetAll();
            ViewBag.titulo = "Catalogo de Profesores";

            return View(Profesores);
        }

        public IActionResult Create()
        {
            ViewBag.titulo = "Agregar Profesor";

            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(Profesor profesor)
        {

            if (ModelState.IsValid)
            {
                //se agrega el profesor
                await _profesorRepository.CreateAsync(profesor);

                return RedirectToAction(nameof(Index));
            }

            ModelState.AddModelError(string.Empty, "Existen Campos Requeridos");
            return View(profesor);

        }

        public async Task<IActionResult> Details(int? id)
        {
            ViewBag.titulo = "Profesor";

            if (id == null)
            {
                return NotFound();
            }

            var profesor = await _profesorRepository.GetByIdAsync(id);

            if (profesor == null)
            {
                return NotFound();
            }

            return View(profesor);

        }


        public async Task<IActionResult> Edit(int? id)
        {
            ViewBag.titulo = "Profesor";

            if (id == null)
            {
                return NotFound();
            }

            var profesor = await _profesorRepository.GetByIdAsync(id);

            if (profesor == null)
            {
                return NotFound();
            }

            return View(profesor);

        }

        [HttpPost]
        public async Task<IActionResult> Edit(Profesor profesor)
        {
            ViewBag.titulo = "Profesor";

            if (ModelState.IsValid)
            {
                await _profesorRepository.UpdateAsync(profesor);

                return RedirectToAction(nameof(Index));

            }

            return View(profesor);

        }

        public async Task<IActionResult> Delete(int id)
        {

            var profesor = await _profesorRepository.GetByIdAsync(id);

            if (profesor == null)
            {
                return NotFound();
            }

            await _profesorRepository.DeleteAsync(profesor);


            return RedirectToAction(nameof(Index));
        }





    }
}
