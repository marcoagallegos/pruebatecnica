﻿using Microsoft.AspNetCore.Mvc;
using PruebaTecnica.Data.Entities;
using PruebaTecnica.Data.Respositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PruebaTecnica.Controllers
{
    public class SeccionesController:Controller
    {
        private readonly ISeccionRepository _seccionRepository;
        private readonly IGradoRepository _gradoRepository;
        private readonly IAlumnoRepository _alumnoRepository;

        public SeccionesController(ISeccionRepository seccionRepository,
                                    IGradoRepository gradoRepository,
                                    IAlumnoRepository alumnoRepository)
        {
            _seccionRepository = seccionRepository;
            _gradoRepository = gradoRepository;
            _alumnoRepository = alumnoRepository;
        }


        public IActionResult Index()
        {

            var secciones = _seccionRepository.GetAllWithAlumnoGrado();


            return View(secciones);
        }


        public IActionResult Create()
        {
            ViewBag.titulo = "Agregar Seccion";

            var gradosList = _gradoRepository.GetGradosList();
            var alumnosList = _alumnoRepository.GetAlumnosList();

            ViewBag.GradosList = gradosList;
            ViewBag.AlumnosList = alumnosList;

            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(AlumnoGrado alumnoGrado)
        {

            if (ModelState.IsValid)
            {



                //se agrega la seccion
                await _seccionRepository.CreateAsync(alumnoGrado);

                return RedirectToAction(nameof(Index));
            }

            ModelState.AddModelError(string.Empty, "Existen Campos Requeridos");
            return View(alumnoGrado);
        }




        public async Task<IActionResult> Edit(int? id)
        {
            ViewBag.titulo = "Seccion";
            var gradosList = _gradoRepository.GetGradosList();
            var alumnosList = _alumnoRepository.GetAlumnosList();

            ViewBag.GradosList = gradosList;
            ViewBag.AlumnosList = alumnosList;


            if (id == null)
            {
                return NotFound();
            }

            var seccion = await _seccionRepository.GetByIdAsync(id);

            if (seccion == null)
            {
                return NotFound();
            }

            return View(seccion);

        }

        [HttpPost]
        public async Task<IActionResult> Edit(AlumnoGrado alumnoGrado)
        {
            ViewBag.titulo = "Seccion";
            var gradosList = _gradoRepository.GetGradosList();
            var alumnosList = _alumnoRepository.GetAlumnosList();

            ViewBag.GradosList = gradosList;
            ViewBag.AlumnosList = alumnosList;


            if (ModelState.IsValid)
            {
                await _seccionRepository.UpdateAsync(alumnoGrado);

                return RedirectToAction(nameof(Index));

            }

            return View(alumnoGrado);

        }


        public async Task<IActionResult> Details(int? id)
        {
            ViewBag.titulo = "Seccion";
            var gradosList = _gradoRepository.GetGradosList();
            var alumnosList = _alumnoRepository.GetAlumnosList();

            ViewBag.GradosList = gradosList;
            ViewBag.AlumnosList = alumnosList;

            if (id == null)
            {
                return NotFound();
            }

            var seccion = await _seccionRepository.GetByIdAsync(id);

            if (seccion == null)
            {
                return NotFound();
            }

            return View(seccion);

        }

        public async Task<IActionResult> Delete(int id)
        {

            var seccion = await _seccionRepository.GetByIdAsync(id);

            if (seccion == null)
            {
                return NotFound();
            }

            await _seccionRepository.DeleteAsync(seccion);


            return RedirectToAction(nameof(Index));
        }
    }
}
