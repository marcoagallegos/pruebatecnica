﻿using Microsoft.AspNetCore.Mvc;
using PruebaTecnica.Data.Entities;
using PruebaTecnica.Data.Respositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PruebaTecnica.Controllers
{
    public class GradosController : Controller
    {
        private readonly IGradoRepository _gradoRepository;
        private readonly IProfesorRepository _profesorRepository;

        public GradosController(IGradoRepository gradoRepository ,
                                IProfesorRepository profesorRepository)
        {
            _gradoRepository = gradoRepository;
            _profesorRepository = profesorRepository;
        }


        public IActionResult Index()
        {

            var grados = _gradoRepository.GetAllWithProfesores();


            return View(grados);
        }


        public IActionResult Create()
        {
            ViewBag.titulo = "Agregar Grado";

            var profesoresList = _profesorRepository.GetProfesoresList();
            ViewBag.profesoresList = profesoresList;
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(Grado grado)
        {

            if (ModelState.IsValid)
            {



                //se agrega el alumno
                await _gradoRepository.CreateAsync(grado);

                return RedirectToAction(nameof(Index));
            }

            ModelState.AddModelError(string.Empty, "Existen Campos Requeridos");
            return View(grado);
        }




        public async Task<IActionResult> Edit(int? id)
        {
            ViewBag.titulo = "Grado";
            var profesoresList = _profesorRepository.GetProfesoresList();
            ViewBag.profesoresList = profesoresList;

            if (id == null)
            {
                return NotFound();
            }

            var grado = await _gradoRepository.GetByIdAsync(id);

            if (grado == null)
            {
                return NotFound();
            }

            return View(grado);

        }

        [HttpPost]
        public async Task<IActionResult> Edit(Grado alumno)
        {
            ViewBag.titulo = "Grado";
            var profesoresList = _profesorRepository.GetProfesoresList();
            ViewBag.profesoresList = profesoresList;

            if (ModelState.IsValid)
            {
                await _gradoRepository.UpdateAsync(alumno);

                return RedirectToAction(nameof(Index));

            }

            return View(alumno);

        }


        public async Task<IActionResult> Details(int? id)
        {
            ViewBag.titulo = "Grado";
            var profesoresList = _profesorRepository.GetProfesoresList();
            ViewBag.profesoresList = profesoresList;

            if (id == null)
            {
                return NotFound();
            }

            var grado = await _gradoRepository.GetByIdAsync(id);

            if (grado == null)
            {
                return NotFound();
            }

            return View(grado);

        }

        public async Task<IActionResult> Delete(int id)
        {

            var grado = await _gradoRepository.GetByIdAsync(id);

            if (grado == null)
            {
                return NotFound();
            }

            await _gradoRepository.DeleteAsync(grado);


            return RedirectToAction(nameof(Index));
        }


    }
}
