﻿using Microsoft.AspNetCore.Mvc;
using PruebaTecnica.Data.Entities;
using PruebaTecnica.Data.Respositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PruebaTecnica.Controllers
{
    public class AlumnosController : Controller
    {
        private readonly IAlumnoRepository _alumnosRepository;

        public AlumnosController( IAlumnoRepository alumnosRepository)
        {
            _alumnosRepository = alumnosRepository;
        }




        public IActionResult Index()
        {
            var alumnos = _alumnosRepository.GetAll();
            ViewBag.titulo = "Catalogo de Alumnos";

            return View(alumnos);
        }

        public IActionResult Create()
        {
            ViewBag.titulo = "Agregar Alumno";

            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create (Alumno alumno)
        {

            if (ModelState.IsValid)
            {
                //se agrega el alumno
                await _alumnosRepository.CreateAsync(alumno);

                return RedirectToAction(nameof(Index));
            }

            ModelState.AddModelError(string.Empty, "Existen Campos Requeridos");
            return View(alumno);
           
        }

        public async Task<IActionResult> Details(int? id)
        {
            ViewBag.titulo = "Alumno";

            if (id == null)
            {
                return NotFound();
            }

            var alumno = await _alumnosRepository.GetByIdAsync(id);

            if (alumno == null)
            {
                return NotFound();
            }

            return View(alumno);

        }


        public async Task<IActionResult> Edit (int? id)
        {
            ViewBag.titulo = "Alumno";

            if (id== null)
            {
                return NotFound();
            }

            var alumno = await _alumnosRepository.GetByIdAsync(id);

            if (alumno == null)
            {
                return NotFound();
            }

            return View(alumno);

        }

        [HttpPost]
        public async Task<IActionResult> Edit(Alumno alumno)
        {
            ViewBag.titulo = "Alumno";

            if (ModelState.IsValid)
            {
                await _alumnosRepository.UpdateAsync(alumno);

                return RedirectToAction(nameof(Index));

            }

            return View(alumno);

        }

        public async Task<IActionResult> Delete(int id)
        {

            var alumno = await _alumnosRepository.GetByIdAsync(id);

            if(alumno == null)
            {
                return NotFound();
            }

            await _alumnosRepository.DeleteAsync(alumno);


            return RedirectToAction(nameof(Index));
        }





    }
}
