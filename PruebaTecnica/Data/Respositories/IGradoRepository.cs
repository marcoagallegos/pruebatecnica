﻿using Microsoft.AspNetCore.Mvc.Rendering;
using PruebaTecnica.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PruebaTecnica.Data.Respositories
{
    public interface IGradoRepository : IGenericRepository<Grado>
    {
        IQueryable<Grado> GetAllWithProfesores();

        IEnumerable<SelectListItem> GetGradosList();
    }
}
