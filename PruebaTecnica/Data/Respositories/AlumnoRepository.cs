﻿using Microsoft.AspNetCore.Mvc.Rendering;
using PruebaTecnica.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PruebaTecnica.Data.Respositories
{
    public class AlumnoRepository : GenericRepository<Alumno>, IAlumnoRepository
    {
        private readonly DataContext _context;

        public AlumnoRepository(DataContext context) : base (context)
        {
            _context = context;
        }

        public IEnumerable<SelectListItem> GetAlumnosList()
        {

            var alumnos = GetAll();
            var list = new List<SelectListItem>();

            list.Add(new SelectListItem
            {
                Text = "Seleccione un Alumno",
                Value = "",
                Selected = true
            });

            foreach (var item in alumnos)
            {
                list.Add(new SelectListItem
                {
                    Text = item.FullName,
                    Value = item.Id.ToString()
                });

            }

            return list;

        }


    }
}
