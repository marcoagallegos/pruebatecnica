﻿using Microsoft.AspNetCore.Mvc.Rendering;
using PruebaTecnica.Data.Entities;
using PruebaTecnica.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PruebaTecnica.Data.Respositories
{
    public class ProfesorRepository : GenericRepository<Profesor>, IProfesorRepository
    {
        private readonly DataContext _context;

        public ProfesorRepository(DataContext context) : base(context)
        {
            _context = context;
        }

        public IEnumerable<SelectListItem> GetProfesoresList()
        {

            var profesores = GetAll();
            var list = new List<SelectListItem>();

            list.Add(new SelectListItem
            {
                Text = "Seleccione un profesor",
                Value = "",
                Selected = true
            });

            foreach (var item in profesores)
            {
                list.Add(new SelectListItem
                {
                    Text = item.FullName,
                    Value = item.Id.ToString()
                });

            }

            return list;

        }

    }
}
