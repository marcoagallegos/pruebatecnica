﻿using Microsoft.AspNetCore.Mvc.Rendering;
using PruebaTecnica.Data.Entities;
using PruebaTecnica.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PruebaTecnica.Data.Respositories
{
    public interface IProfesorRepository : IGenericRepository<Profesor>
    {

        IEnumerable<SelectListItem> GetProfesoresList();
    }
}
