﻿using Microsoft.EntityFrameworkCore;
using PruebaTecnica.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PruebaTecnica.Data.Respositories
{
    public class SeccionRepository : GenericRepository<AlumnoGrado>, ISeccionRepository
    {
        private readonly DataContext _context;

        public SeccionRepository(DataContext context) : base (context)
        {
            _context = context;
        }

        public IQueryable<AlumnoGrado> GetAllWithAlumnoGrado()
        {
            var secciones = _context.AlumnoGrado
                            .Include(x => x.Alumno)
                            .Include(x=> x.Grado)
            ;

            return secciones;

        }


    }
}
