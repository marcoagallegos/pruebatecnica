﻿using PruebaTecnica.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PruebaTecnica.Data.Respositories
{
    public interface ISeccionRepository : IGenericRepository<AlumnoGrado>
    {
        IQueryable<AlumnoGrado> GetAllWithAlumnoGrado();
    }
}
