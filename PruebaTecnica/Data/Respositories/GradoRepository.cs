﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using PruebaTecnica.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PruebaTecnica.Data.Respositories
{
    public class GradoRepository : GenericRepository<Grado>, IGradoRepository
    {
        private readonly DataContext _context;

        public GradoRepository(DataContext context) : base (context)
        {
            _context = context;
        }

        public IQueryable<Grado> GetAllWithProfesores()
        {
            var grados = _context.Grado
                            .Include(x => x.Profesor)
                            ;

            return grados;

        }

        public IEnumerable<SelectListItem> GetGradosList()
        {

            var grados = GetAll();
            var list = new List<SelectListItem>();

            list.Add(new SelectListItem
            {
                Text = "Seleccione un Grado",
                Value = "",
                Selected = true
            });

            foreach (var item in grados)
            {
                list.Add(new SelectListItem
                {
                    Text = item.Nombre,
                    Value = item.Id.ToString()
                });

            }

            return list;

        }


    }
}
