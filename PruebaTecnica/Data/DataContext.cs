﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using PruebaTecnica.Data.Entities;
using System.Collections.Generic;
using System.Linq;

namespace PruebaTecnica.Data
{
    public class DataContext : IdentityDbContext
        {
            public DataContext(DbContextOptions<DataContext> options)
                : base(options)
            {
                        


            }



        public DbSet<Alumno> Alumno { get; set; }

        public DbSet<Profesor> Profesor { get; set; }

        public DbSet<Grado> Grado { get; set; }

        public DbSet<AlumnoGrado> AlumnoGrado { get; set; }


        protected override void OnModelCreating(ModelBuilder builder)
        {


            //deshabilita el borrado en cascada de la bd
            IEnumerable<Microsoft.EntityFrameworkCore.Metadata.IMutableForeignKey> cascadeFKs = builder.Model
                .GetEntityTypes()
                .SelectMany(t => t.GetForeignKeys())
                .Where(fk => !fk.IsOwnership && fk.DeleteBehavior == DeleteBehavior.Cascade);

            foreach (Microsoft.EntityFrameworkCore.Metadata.IMutableForeignKey fk in cascadeFKs)
            {
                fk.DeleteBehavior = DeleteBehavior.Restrict;
            }


            base.OnModelCreating(builder);
        }


    }
    }
