﻿using System;
using System.ComponentModel.DataAnnotations;

namespace PruebaTecnica.Data.Entities
{
    public class Alumno :IEntity
    {
        public int Id { get; set; }

        [Display(Name = "Nombre", Prompt ="Escriba el Nombre")]
        [Required]
        public string Nombre { get; set; }

        [Display(Name = "Apellidos" , Prompt = "Escriba el Apellido")]
        [Required]
        public string Apellidos { get; set; }

        [Display(Name = "Genero" )]
        [Required]                
        public string Genero { get; set; }

        [Display(Name = "Fecha de Nacimiento")]
        [Required]
        [DataType(DataType.Date)]
        public DateTime FechaNacimiento { get; set; }

        public string FullName { get { return Nombre + " " + Apellidos; } }

    }
}
