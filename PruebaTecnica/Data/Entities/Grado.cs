﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace PruebaTecnica.Data.Entities
{
    public class Grado :IEntity
    {
        public int Id { get; set; }

        [Display(Name = "Nombre:", Prompt = "Escriba el Nombre del Grado")]
        [Required]
        public string Nombre { get; set; }

        [Display(Name = "Profesor:")]
      
        public Profesor Profesor { get; set; }

        
        public int ProfesorId { get; set; }

    }
}
