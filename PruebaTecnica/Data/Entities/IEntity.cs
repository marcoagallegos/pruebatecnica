﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PruebaTecnica.Data.Entities
{
    public interface IEntity
    {

        public int Id { get; set; }
    }
}
