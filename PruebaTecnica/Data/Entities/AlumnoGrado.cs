﻿using System.ComponentModel.DataAnnotations;

namespace PruebaTecnica.Data.Entities
{
    public class AlumnoGrado :IEntity
    {

        public int Id { get; set; }

        public Alumno Alumno { get; set; }

        public Grado Grado { get; set; }

        [Required]
        public string Seccion { get; set; }

        
        public int AlumnoId { get; set; }

        public int GradoId { get; set; }

    }
}
